// author : wangzhen
// time : 2017-06-07
$(function(){
    $('.wrap .content .left .item:last-child').find('span').addClass('round-solid-hover');
    $('.wrap .content .left .item:last-child').find('div.line').addClass('line-hidden');
    $('.wrap .content .left .item:last-child').find('a').css('color', '#5DD1FD');

    $('.wrap .content .right .item:last-child').siblings('.item').hide();

    // click left item
    $(document).on('click', '.wrap .content .left .item p', function(){
        var $this = $(this);
        $this.children('a').css('color', '#5DD1FD');
        $this.children('span.round').addClass('round-solid-hover');
        $this.parent().siblings().find('span').removeClass('round-solid-hover');
        $this.parent().siblings().find('a').css('color', '#666');
        var indexIs = $this.parent().index();
        var that = $('.wrap .content .right .item').eq(indexIs);
        that.show();
        that.siblings('.item').hide();

    });
});